#!/usr/bin/python3
#
# This file is part of FreedomBox.
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
#

import argparse
import xml.etree.ElementTree as etree


def parse_arguments():
    """Return parsed command line arguments as dictionary."""
    parser = argparse.ArgumentParser()
    subparsers = parser.add_subparsers(dest='subcommand', help='Sub command')

    subparser = subparsers.add_parser(
        'remove-footer', help='Remove footer from the XML document')
    subparser.add_argument('filename', help='Name of the XML file')

    subparser = subparsers.add_parser('fix-wiki-urls',
                                      help='Fix wrongly formatted wiki urls')
    subparser.add_argument('filename', help='Name of the Docbook file')

    subparsers.required = True
    return parser.parse_args()


def subcommand_fix_wiki_urls(arguments):
    file_name = arguments.filename
    page_name = file_name.split('.')[0]

    with open(file_name, 'r') as xml_file:
        lines = xml_file.readlines()

    pattern = f'FreedomBox/Manual/{page_name}/FreedomBox'
    lines = list(map(lambda s: s.replace(pattern, 'FreedomBox'), lines))

    with open(file_name, 'w') as xml_file:
        xml_file.writelines(lines)


def subcommand_remove_footer(arguments):
    """Remove the footer template from the given wiki page."""
    filename = arguments.filename
    tree = etree.parse(filename)
    root = tree.getroot()

    # The footer will always be in the last <section>
    def find_last_section(elem):
        if len(elem):
            last_element = elem[-1]
            if last_element.tag == 'section':
                return find_last_section(last_element)
        return elem

    last_section = find_last_section(root)

    if len(last_section):
        # Remove all elements till <informaltable> is reached
        while last_section[-1].tag != 'informaltable':
            last_section.remove(last_section[-1])
        # remove <informaltable> itself
        last_section.remove(last_section[-1])

        # Remove the line "Back to Features introduction or manual pages."
        if last_section[-1].text.startswith('Back to'):
            last_section.remove(last_section[-1])

    processed_xml = etree.tostring(root, encoding='utf-8').decode()

    with open(filename, 'r') as xml_file:
        # <xml> and <DOCTYPE> elements which etree skips
        header = xml_file.readlines()[:2]

    with open(filename, 'w') as xml_file:
        xml_file.writelines(header)
        xml_file.write(processed_xml)


def main():
    """Parse arguments and perform all duties."""
    arguments = parse_arguments()

    subcommand = arguments.subcommand.replace('-', '_')
    subcommand_method = globals()['subcommand_' + subcommand]
    subcommand_method(arguments)


if __name__ == '__main__':
    main()
